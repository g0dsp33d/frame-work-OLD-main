$(document).ready(function () {
$('.owler').owlCarousel({
  animateOut: 'slideOutDown',
  animateIn: 'slideInDown',
  stagePadding: 50,
  smartSpeed: 1950,
  margin:0,
  loop:true,
  lazyLoad: true,
  items:3
});
$('.lightgallery').lightGallery();
$('body').flowtype({
  minimum: 320,
  maximum: 1960,
  minFont: 10,
  maxFont: 20,
  fontRatio: 25
});
$('.venobox').venobox();
});
